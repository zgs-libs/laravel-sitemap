<?php
namespace Mrlaozhou\Sitemap;

use Illuminate\Support\Facades\File;
use Mrlaozhou\Extend\Collection;

class Sitemap
{

    /**
     * @param $config
     *
     * @return bool|int
     */
    public function store($config, $keyword)
    {
        $collection     =   $this->collect($config['handler']);
        $title          =   $config['title'] ?? '';
        $this->htmlStore($collection, $title, $keyword);
        $this->xmlStore($collection, $title, $keyword);
    }

    public function htmlStore($collection, $title, $keyword)
    {
        return File::put( public_path($keyword . '.html'), view( 'vendor.sitemap.html', [
            'collection'        =>  $collection,
            'title'             =>  $title,
        ] ) );
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param $title
     * @param $keyword
     *
     * @return bool|int
     */
    public function xmlStore($collection, $title, $keyword)
    {
        $collection     =   collect($collection)->pluck('children')
            ->flatten()->filter()
            ->merge($collection)->toArray();
        return File::put( public_path($keyword . '.xml'), view('vendor.sitemap.xml', [
            'collection'        =>  $collection,
            'title'             =>  $title,
        ]) );
    }

    /**
     * @param $handler string
     *
     * @return array|\Illuminate\Contracts\Support\Arrayable
     */
    public function collect($handler)
    {
        if( class_exists( $handler ) && ( ($handlerInstance = new $handler) instanceof HandlerContract ) ) {
            //  获取数据
            /**
             * @var \Mrlaozhou\Extend\Collection $collection
             * @var \Mrlaozhou\Sitemap\HandlerContract $handlerInstance
             */
            $collection           =   $handlerInstance->handle();
            return $collection;
        }
        return [];
    }
}
