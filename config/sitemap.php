<?php

return [
    /*
    |--------------------------------------------------------------------------
    | -- 配置 --
    |--------------------------------------------------------------------------
    |   -- 描述 --
    |
    */

    'keywords'          =>  [
        //  通用
        'sitemap'              =>  [
            'title'         =>  'Sitemap',
            'handler'       =>  \Mrlaozhou\Sitemap\SiteMapHandler::class,
        ],

        //  手机端
        'mobile'                =>  [
            'title'         =>  '手机端',
            'handler'       =>  \Mrlaozhou\Sitemap\SiteMapHandler::class,
        ],

        //  PC端
        'computer'              =>  [
            'title'         =>  'PC端',
            'handler'       =>  \Mrlaozhou\Sitemap\SiteMapHandler::class,
        ],
    ]
];
